cki-kcidb-pump
==============

Cki-kcidb-pump is a tool for transferring test results from the CKI data
warehouse to the KernelCI common report database (kcidb), either continuously
or single-shot.

Installation
------------
First, install [kcidb][kcidb]:

    pip3 install --user git+https://github.com/kernelci/kcidb.git

Then you can run cki-kcidb-pump directly, like this:

    ./cki-kcidb-pump --help

or put it into one of the directories in your `$PATH` and run normally:

    cki-kcidb-pump --help

Next, create and initialize your own dataset in BigQuery, and/or acquire
Google application credentials file for access, and specify it for use with
Google libraries. Like this:

    export GOOGLE_APPLICATION_CREDENTIALS=<CREDENTIALS_FILE>

where `<CREDENTIALS_FILE>` is the credentials file. E.g.:

    export GOOGLE_APPLICATION_CREDENTIALS=~/.bq.json

See more setup and dataset management instructions in kcidb
[README.md][kcidb_readme_md].

Usage
-----

Run `cki-kcidb-pump --help` for a command-line interface summary.

For any transfer you have to specify the source and the destination.

The source is the base URL for a set of data warehouse REST endpoints exposing
result data in kcidb format. The endpoints should be reachable by appending a
slash and a collection name (e.g. `revisions`, `builds`, or `tests`) to the
base URL. E.g. for these three endpoints:

    http://datawarehouse.example.com/api/1/kcidb/data/revisions
    http://datawarehouse.example.com/api/1/kcidb/data/builds
    http://datawarehouse.example.com/api/1/kcidb/data/tests

The base URL will be this:

    http://datawarehouse.example.com/api/1/kcidb

The target is the name of the Google Cloud project hosting the data, plus the
name of the Pub/Sub topic within that project, accepting the submissions.
E.g. `kernelci` and `kernelci_new`.

Having both of these, you can transfer all the data in the data warehouse in
one go, like this:

    cki-kcidb-pump http://datawarehouse.example.com/kcidb/1/data \
                   kernelci kernelci_new

Add a polling period option for a continuous transfer:

    cki-kcidb-pump --period 60 \
                   http://datawarehouse.example.com/kcidb/1/data \
                   kernelci kernelci_new

The above would transfer all the data in the data warehouse, and then poll for
new data every minute, and continue transferring it as it appears.

Add a minimum timestamp to start transferring from a certain time in report
history:

    cki-kcidb-pump --period 60 \
                   --time-min "`date --rfc-3339=s --date '1 week ago'`" \
                   http://datawarehouse.example.com/kcidb/1/data \
                   kernelci kernelci_new

The above would transfer continuously starting with reports created a week
before the execution time.

Finally, specify a cursor file to be able to keep track of which data was
transferred already and which not, across multiple invocations, and to be able
to interrupt and resume transfers:

    cki-kcidb-pump --period 60 \
                   --time-min "`date --rfc-3339=s -d '1 week ago'` \
                   --cursor cursor.json \
                   http://datawarehouse.example.com/kcidb/1/data \
                   kernelci kernelci_new

The above would start transferring data from a week ago, or from the position
stored in `cursor.json`, if it existed. It will also create/update
`cursor.json` with the new position, after every piece of data successfully
transferred.

At the moment, cki-kcidb-pump produces a lot of debug output with no way to
control it (to be improved as necessary). If you'd like to add timestamps to
that output you can use `unbuffer` (from `expect`) and `ts` (from
`moreutils`) like this:

    unbuffer \
        cki-kcidb-pump \
            --period 60 \
            --time-min "`date --rfc-3339=s -d '1 week ago'` \
            --cursor cursor.json \
            http://datawarehouse.example.com/kcidb/1/data \
            kernelci kernelci_new |
    ts


[kcidb]: https://github.com/kernelci/kcidb/
[kcidb_readme_md]: https://github.com/kernelci/kcidb/blob/master/README.md
